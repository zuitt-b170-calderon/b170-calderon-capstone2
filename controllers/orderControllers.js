//set up dependencies
const User = require("../models/User.js")
const Product = require("../models/Product.js")
const Order = require("../models/Order.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt")

// Non Admin User checkout (create order)

module.exports.userCheckout = async(data)=>{
	let isUserUpdated = await User.findById(data.userId).then(user =>{
		user.purchases.push( {productId:data.productId} );

		return user.save().then((user,err)=>{
			if (err){
				return false
			} else {
				return true
			}
		})
	});
	
	let isOrderUpdate = await Product.findById(data.productId).then(product =>{
		order.purchases.push( {
			productId:data.userId,
			price: data.price
		})
		return order.save().then((product,error)=>{
			if (error){
				return false
			}else {
				return true
			}
		})
	})

	if(isUserUpdated && isOrderUpdate){
		return true
	}else {
		return false
	}
}
