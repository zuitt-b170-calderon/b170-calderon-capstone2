//set up dependencies
const User = require("../models/User.js")
const Product = require("../models/Product.js")
const Order = require("../models/Order.js")
const auth = require("../auth.js")
const bcrypt = require("bcrypt")

//User Registration

module.exports.registerUser=(reqBody)=>{
	let newUser = new User({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password,10),
	})
	return newUser.save().then((saved,error)=>{
		if(error){
			console.log(error)
			return false
		}else{
			return true
		}
	})
}

//User Authentication

module.exports.userAuthentication = (reqBody) => {
	return User.findOne( { email: reqBody.email } ).then(result => {
		if (result === null) {
			return false
		} else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)

			if (isPasswordCorrect) {
				return {access: auth.createAccessToken(result.toObject())}
			} else {
				return false
			}
		}
	})
}

//Set user Admin (Admin Only)

<<<<<<< HEAD
module.exports.userAdminUpdate = (data, reqParams)=>{
	let isUserAdmin = {
			isAdmin: true
		}
		if(data.isAdmin === true){
			return User.findByIdAndUpdate(reqParams.id, isUserAdmin).then((result,error)=>{
				if(error){
					return false
				} else {
					return true
				}
			})
		} else {
			return false;
			console.log("You are not an Admin")
		}
}		
=======
module.exports.userAdminUpdate = (reqBody, userData)=>{
	return User.findById(userData.userId).then(result => {
		if (userData.isAdmin === false) {
			return "You are not an Admin"
		} else {
			let newAdmin = new User({
				isAdmin: true
			})

			return newAdmin.save().then((user, error)=>{
				if (error){
					return false
				} else {
					return "New Admin created successfully"
				}
			})
		}
	})
}

>>>>>>> 4cd13575c546514e7614800d80f3971edbbee814

