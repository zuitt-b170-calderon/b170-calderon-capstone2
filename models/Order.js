const mongoose = require("mongoose")

const orderSchema = new mongoose.Schema({
	totalAmount:{
		type: Number,
		required: [true, "Amount is required"]
	},
	userId:{
		type: String,
		required: [true,"User Id is required"]
	},
	purchases:[
	{
		productId:{
			type: String,
			required: [true,"Product Id is required"]
		},
		price:{
			type: Number,
			required: [true, "Price of product is required"]
		}
		createdOn:{
			type: Date,
			default: new Date()
		},
	}
	] 
})

module.exports = mongoose.model("Orders", orderSchema);

<<<<<<< HEAD
=======
/*const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CartSchema = new Schema({
	owner: {type: Schema.Types.ObjectId, ref: 'User'},
	total: {type: Number, default: 0},
	items: [{
		item: {type: Schema.Types.ObjectId, ref: 'Product'},
		quantity: {type: Number, default: 1},
		price: {type: Number, default: 0}
	}]
});
*/
>>>>>>> 4cd13575c546514e7614800d80f3971edbbee814
