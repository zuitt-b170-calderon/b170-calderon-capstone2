const express = require("express")
const router = express.Router()

const auth = require("../auth.js")
const productController = require("../controllers/productControllers.js")


// retrieve all active products

router.get("/active", (req, res) => {
	productController.getActiveProducts().then(resultFromController => res.send(resultFromController))
})

// retrieve single product

<<<<<<< HEAD
router.get("/:id",  (req, res) => {
	productController.getProduct(req.params.id).then(result => res.send(result))
=======
router.get("/:productId",  (req, res) => {
	console.log(req.params.productId);
	productController.getProduct(req.params.productId).then(result => res.send(result))
>>>>>>> 4cd13575c546514e7614800d80f3971edbbee814
})

//create product (Admin only)

router.post("/", auth.verify, (req, res) => {
    const userData = auth.decode(req.headers.authorization) 
    productController.addProduct(req.body, userData).then(resultFromController => res.send(resultFromController))
})

//update product information (Admin only)

<<<<<<< HEAD
router.put("/:id", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization) 
	productController.updateProduct(user, req.params.id, req.body).then(result => res.send(result))
})

// Archive Product (Admin Only)

router.put("/archive/:id", auth.verify,(req,res)=>{
	productController.archivedProduct(req.params, req.body).then(result=>res.send(result))
})
=======
router.put("/:productId", auth.verify, (req, res) => {
	productController.updateProduct(req.params, req.body).then(result => res.send(result))
})


>>>>>>> 4cd13575c546514e7614800d80f3971edbbee814


module.exports = router